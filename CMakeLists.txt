cmake_minimum_required(VERSION 3.4)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/Lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/Lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/Bin)

	
link_directories(
	3rdParty/wxWidgets/lib/vc140_dll
	)

MACRO(ADD_MSVC_PRECOMPILED_HEADER PrecompiledHeader PrecompiledSource SourcesVar)
  IF(MSVC)
	GET_FILENAME_COMPONENT(PrecompiledBasename ${PrecompiledHeader} NAME_WE)
	SET(PrecompiledBinary "${CMAKE_CURRENT_BINARY_DIR}/${PrecompiledBasename}.pch")
	SET(Sources ${${SourcesVar}})

	SET_SOURCE_FILES_PROPERTIES(${PrecompiledSource}
								PROPERTIES COMPILE_FLAGS "/Yc\"${PrecompiledHeader}\" /Fp\"${PrecompiledBinary}\""
										   OBJECT_OUTPUTS "${PrecompiledBinary}")
	SET_SOURCE_FILES_PROPERTIES(${Sources}
								PROPERTIES COMPILE_FLAGS "/Yu\"${PrecompiledHeader}\" /FI\"${PrecompiledHeader}\" /Fp\"${PrecompiledBinary}\""
										   OBJECT_DEPENDS "${PrecompiledBinary}")  
	# Add precompiled header to SourcesVar
	LIST(APPEND ${SourcesVar} ${PrecompiledSource})
  ENDIF(MSVC)
ENDMACRO(ADD_MSVC_PRECOMPILED_HEADER)

project(DoseEffect)

add_definitions(-D_CRT_SECURE_NO_WARNINGS)

set(SOURCES
	Source/Application.cpp
	Source/Application.h
	Source/MainWindow.cpp
	Source/MainWindow.h
	Source/StdAfx.h
	Source/DataModel.cpp
	Source/DataModel.h
	Source/Experiment.cpp
	Source/Experiment.h
	Source/MathPlot/MathPlot.cpp
	Source/MathPlot/MathPlot.h
	Source/Kernels.h
	Source/Kernels.cpp
	)

ADD_MSVC_PRECOMPILED_HEADER("StdAfx.h" "Source/StdAfx.cpp" SOURCES)

source_group("Source" FILES ${SOURCES})

add_executable(DoseEffect WIN32
	${SOURCES}
)

set_property(TARGET DoseEffect PROPERTY CXX_STANDARD 11)

target_include_directories(DoseEffect PRIVATE
	Source
	3rdParty/json
	3rdParty/wxWidgets/include
	3rdParty/wxWidgets/lib/vc140_dll/mswud
	)

target_link_libraries(DoseEffect
	wxbase31ud
	# wxbase31ud_net
	# wxbase31ud_xml
	wxmsw31ud_adv
	# wxmsw31ud_aui
	wxmsw31ud_core
	# wxmsw31ud_gl
	# wxmsw31ud_html
	# wxmsw31ud_media
	# wxmsw31ud_propgrid
	# wxmsw31ud_qa
	# wxmsw31ud_ribbon
	# wxmsw31ud_richtext
	# wxmsw31ud_stc
	# wxmsw31ud_webview
	# wxmsw31ud_xrc	
	)

set(DLLS_TO_BE_COPIED
	3rdParty/wxWidgets/lib/vc140_dll/wxbase310ud_vc140.dll
	3rdParty/wxWidgets/lib/vc140_dll/wxmsw310ud_adv_vc140.dll
	3rdParty/wxWidgets/lib/vc140_dll/wxmsw310ud_core_vc140.dll
	)

foreach(DLL ${DLLS_TO_BE_COPIED})
	file(COPY ${DLL} DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/Debug)
	file(COPY ${DLL} DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/Release)
endforeach()