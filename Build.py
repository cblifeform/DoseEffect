import os

BUILD_DIR = 'Build'

def main():
	if not os.path.exists(BUILD_DIR):
		os.mkdir('Build')
	os.chdir('Build')

	os.system('cmake .. -G "Visual Studio 14 2015 Win64"')

if __name__ == '__main__':
	main()
