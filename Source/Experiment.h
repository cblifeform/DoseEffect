#pragma once
#include <DataModel.h>
#include <Kernels.h>

class Experiment
{
public:
	void Initialize(std::shared_ptr<DataModel> dataModel, Kernel kernel);
	void Run();

	std::vector<double> X;
	std::vector<double> Y;
private:
	double NadarayaWatson(double x);
	void CalculateBandwidth();
	double LOO(double h);
	double Kh(double x, double h);

	std::shared_ptr<DataModel> mDataModel;
	Kernel mKernel;
	double mH = 1.0;

};