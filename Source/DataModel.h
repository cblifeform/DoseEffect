#pragma once
#include <StdAfx.h>

class DataModel
{
public:
	struct Data
	{
		double U;
		double X;
		double W;
		double Eps;
	};

	void GenerateRandomData(double minX, double maxX, double minU, double maxU, size_t dataSize = 100);
	void LoadFromFile(const std::string& filename);
	void SaveToFile(const std::string& filename);
	const std::vector<Data>& GetData() const { return mData; };

private:
	std::vector<Data> mData;
};