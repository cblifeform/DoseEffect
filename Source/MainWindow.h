#pragma once
#include <MathPlot/MathPlot.h>
#include <DataModel.h>
#include <Experiment.h>


class MainWindow : public wxFrame
{
public:
	MainWindow();

private:
	void InitializeControlsPanel(wxWindow* parent);

	void InitializePlot();
	void UpdateDataModelView();
	void UpdateNWView();

	mpWindow* dataModelPlot = nullptr;
	mpWindow* NWPlot = nullptr;
	std::shared_ptr<DataModel> mDataModel;
	std::shared_ptr<Experiment> mExperiment;
	wxSpinCtrl*	mSpinSampleCount = nullptr;

	wxSpinCtrl*	mMinX = nullptr;
	wxSpinCtrl*	mMaxX = nullptr;
	wxSpinCtrl*	mMinU = nullptr;
	wxSpinCtrl*	mMaxU = nullptr;

	wxGrid*		mGrid = nullptr;
	wxChoice*	cmbKernel = nullptr;
};