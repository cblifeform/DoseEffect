#pragma once

#include <stdint.h>

#include <string>
#include <fstream>
#include <functional>
#include <thread>
#include <mutex>
#include <random>
#include <sstream>
//#define NOMINMAX
//#include <Windows.h>

#define check(expression) if (!(expression)) { throw std::runtime_error("Error in " __FILE__ ": " #expression);}

#define SAFE_GL(expression) {expression; if (glGetError() != GL_NO_ERROR) { throw std::runtime_error("OpenGL error occured!"); }}

#define WXUSINGDLL 1
#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/grid.h>