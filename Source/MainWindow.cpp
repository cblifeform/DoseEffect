﻿#include <StdAfx.h>
#include <MainWindow.h>

MainWindow::MainWindow()
	: wxFrame(nullptr, wxID_ANY, _T("Доза-эффект"), wxDefaultPosition, wxSize(1024, 768))
{
	//this->SetBackgroundColour(wxColor::)
	mDataModel = std::make_shared<DataModel>();
	mDataModel->GenerateRandomData(0.0, 6.0, 0.0, 8.0);

	mExperiment = std::make_shared<Experiment>();
	mExperiment->Initialize(mDataModel, EpanechkinKernel);
	mExperiment->Run();
	
	wxSizer* mainSizer = new wxBoxSizer(wxHORIZONTAL);


	wxPanel* panel = new wxPanel(this);
	InitializeControlsPanel(panel);

	wxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	mainSizer->Add(panel, 1, wxEXPAND);

	//mainSizer->Add(controlsSizer);
	mainSizer->Add(sizer, 4, wxEXPAND);
	
	SetSizer(mainSizer);

	dataModelPlot = new mpWindow(this, -1, wxPoint(0, 0), wxDefaultSize, wxSUNKEN_BORDER);
	NWPlot = new mpWindow(this, -1, wxPoint(0, 0), wxDefaultSize, wxSUNKEN_BORDER);
	sizer->Add(dataModelPlot, 1, wxEXPAND);
	sizer->Add(NWPlot, 1, wxEXPAND);

	UpdateDataModelView();
	UpdateNWView();

}

void MainWindow::InitializeControlsPanel(wxWindow* parent)
{
	wxSizer* mainSizer = new wxBoxSizer(wxHORIZONTAL);
	wxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	wxSizer* gridSizer = new wxBoxSizer(wxVERTICAL);

	parent->SetSizer(mainSizer);
	mainSizer->Add(sizer, 1, wxALL | wxEXPAND, 5);
	

	sizer->Add(new wxStaticText(parent, -1, _("Размер выборки:")));
	mSpinSampleCount = new wxSpinCtrl(parent, -1);
	mSpinSampleCount->SetValue(100);
	mSpinSampleCount->SetMax(100500);
	sizer->Add(mSpinSampleCount, wxSizerFlags().Expand());

	sizer->Add(new wxStaticText(parent, -1, _("Параметры X:")));
	mMinX = new wxSpinCtrl(parent, -1);
	mMaxX = new wxSpinCtrl(parent, -1);
	mMaxX->SetValue(6);
	sizer->Add(mMinX, wxSizerFlags().Expand());
	sizer->Add(mMaxX, wxSizerFlags().Expand());

	sizer->Add(new wxStaticText(parent, -1, _("Параметры U:")));
	mMinU = new wxSpinCtrl(parent, -1);
	mMaxU = new wxSpinCtrl(parent, -1);
	mMaxU->SetValue(8);
	sizer->Add(mMinU, wxSizerFlags().Expand());
	sizer->Add(mMaxU, wxSizerFlags().Expand());

	wxButton* btnGenerate = new wxButton(parent, -1, _("Генерировать выборку"));
	btnGenerate->Bind(wxEVT_BUTTON, [this](wxEvent&)
	{
		mDataModel->GenerateRandomData((double)mMinX->GetValue(), (double)mMaxX->GetValue(),
										(double)mMinU->GetValue(), (double)mMaxU->GetValue(),
										mSpinSampleCount->GetValue());
		UpdateDataModelView();
	});

	wxButton* saveData = new wxButton(parent, -1, _("Сохранить выборку"));
	wxButton* loadData = new wxButton(parent, -1, _("Загрузить выборку"));

	sizer->Add(btnGenerate, wxSizerFlags().Expand());
	sizer->Add(saveData, wxSizerFlags().Expand());
	sizer->Add(loadData, wxSizerFlags().Expand());
	sizer->Add(new wxStaticText(parent, -1, _("Используемое ядро:")));


	wxArrayString choices;
	choices.Add(_("Епанечников"));
	choices.Add(_("Квартическое"));
	choices.Add(_("Равномерное"));
	choices.Add(_("Треугольное"));
	choices.Add(_("Косинус-ядро"));
	choices.Add(_("Лапласа"));
	choices.Add(_("Гауссово"));

	cmbKernel = new wxChoice(parent, -1, wxDefaultPosition, wxDefaultSize, choices);
	cmbKernel->Select(0);
	sizer->Add(cmbKernel, wxSizerFlags().Expand());
	sizer->Add(new wxStaticText(parent, -1, _("Шаг h (оставьте пустым для автоматического подбора):")));
	wxTextCtrl* mText = new wxTextCtrl(parent, -1);
	sizer->Add(mText, 0, wxEXPAND);
	wxButton* btnBuild = new wxButton(parent, -1, _("Провести оценку Надарая — Ватсона"));
	btnBuild->Bind(wxEVT_BUTTON, [this](wxEvent&)
	{
		mExperiment->Initialize(mDataModel, GetKernelByIndex(cmbKernel->GetSelection()));
		mExperiment->Run();
		UpdateNWView();
	});
	sizer->Add(btnBuild, wxSizerFlags().Expand());

	wxButton* btnKnn = new wxButton(parent, -1, _("Провести knn-оценку"));
	btnKnn->Bind(wxEVT_BUTTON, [this](wxEvent&)
	{
		mExperiment->Initialize(mDataModel, GetKernelByIndex(cmbKernel->GetSelection()));
		mExperiment->Run();
		UpdateNWView();
	});
	sizer->Add(btnKnn, wxSizerFlags().Expand());

	mGrid = new wxGrid(parent, -1, wxDefaultPosition, wxSize(300, -1));
	gridSizer->Add(mGrid, 1, wxALL | wxEXPAND, 5);


	mainSizer->Add(gridSizer, 1, wxALL | wxEXPAND, 5);
	Layout();
}
void MainWindow::InitializePlot()
{

}

void MainWindow::UpdateDataModelView()
{
	dataModelPlot->DelAllLayers(true);
	mpScaleX* uAxis = new mpScaleX(wxT("U"), mpALIGN_BOTTOM, true, mpX_NORMAL);
	mpScaleY* xAxis = new mpScaleY(wxT("X"), mpALIGN_LEFT, true);
	wxFont graphFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
	uAxis->SetFont(graphFont);
	xAxis->SetFont(graphFont);
	uAxis->SetDrawOutsideMargins(false);
	xAxis->SetDrawOutsideMargins(false);
	dataModelPlot->SetMargins(30, 30, 50, 100);
	dataModelPlot->AddLayer(uAxis);
	dataModelPlot->AddLayer(xAxis);
	
	std::vector<double> UDataW1;
	std::vector<double> XDataW1;
	std::vector<double> UDataW0;
	std::vector<double> XDataW0;


	mGrid->SetTable(nullptr);
	mGrid->CreateGrid(mDataModel->GetData().size(), 2);
	mGrid->SetColLabelValue(0, _("U"));
	mGrid->SetColLabelValue(1, _("X"));

	size_t rowNum = 0;
	for (const DataModel::Data& data : mDataModel->GetData())
	{
		mGrid->SetCellValue(rowNum, 0, std::to_string(data.U));
		mGrid->SetCellValue(rowNum, 1, std::to_string(data.X));
		++rowNum;

		if (data.U > data.X)
		{
			UDataW1.push_back(data.U);
			XDataW1.push_back(data.X);
		}
		else
		{
			UDataW0.push_back(data.U);
			XDataW0.push_back(data.X);
		}
	}
	mpFXYVector* vectorLayerW1 = new mpFXYVector();
	vectorLayerW1->SetData(UDataW1, XDataW1);
	vectorLayerW1->SetContinuity(false);
	vectorLayerW1->SetPen(wxPen(wxColor(0, 0, 255), 5));
	dataModelPlot->AddLayer(vectorLayerW1);

	mpFXYVector* vectorLayerW0 = new mpFXYVector();
	vectorLayerW0->SetData(UDataW0, XDataW0);
	vectorLayerW0->SetContinuity(false);
	vectorLayerW0->SetPen(wxPen(wxColor(255, 0, 0), 5));
	dataModelPlot->AddLayer(vectorLayerW0);

	dataModelPlot->Fit();

	
}

void MainWindow::UpdateNWView()
{	
	NWPlot->DelAllLayers(true);
	mpScaleX* uAxis = new mpScaleX(wxT("U"), mpALIGN_BOTTOM, true, mpX_NORMAL);
	mpScaleY* xAxis = new mpScaleY(wxT("W"), mpALIGN_LEFT, true);
	wxFont graphFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
	uAxis->SetFont(graphFont);
	xAxis->SetFont(graphFont);
	uAxis->SetDrawOutsideMargins(false);
	xAxis->SetDrawOutsideMargins(false);
	NWPlot->SetMargins(30, 30, 50, 100);
	NWPlot->AddLayer(uAxis);
	NWPlot->AddLayer(xAxis);


	mpFXYVector* vectorLayer = new mpFXYVector();
	vectorLayer->SetData(mExperiment->X, mExperiment->Y);
	vectorLayer->SetContinuity(true);
	vectorLayer->SetPen(wxPen(wxColor(0, 0, 255), 1));
	NWPlot->AddLayer(vectorLayer);

	NWPlot->Fit();
}
