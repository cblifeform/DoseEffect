#pragma once
#include <StdAfx.h>

using Kernel = std::function<double(double x)>;

double EpanechkinKernel(double x);
double QuarticKernel(double x);
double UniformKernel(double x);
double TriangleKernel(double x);
double CosineKernel(double x);
double LaplaceKernel(double x);
double GaussKernel(double x);

Kernel GetKernelByIndex(size_t kernelIndex);