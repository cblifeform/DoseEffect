#include "Experiment.h"

void Experiment::Initialize(std::shared_ptr<DataModel> dataModel, Kernel kernel)
{
	mDataModel = dataModel;
	mKernel = kernel;
}

void Experiment::Run()
{
	X.clear();
	Y.clear();

	auto& data = mDataModel->GetData();
	for (double x = data.front().U; x < data.back().U; x += 0.01)
	{
		double F = NadarayaWatson(x);
		X.push_back(x);
		Y.push_back(F);
	}
}

double Experiment::NadarayaWatson(double x)
{
	const std::vector<DataModel::Data>& dataVector = mDataModel->GetData();
	const double H = 1.40763;



	double S1 = 0.0;
	double S2 = 0.0;
	for (size_t i = 0; i < dataVector.size(); ++i)
	{
		const DataModel::Data& data = dataVector[i];
		S1 += Kh(data.U - x, mH);
		S2 += data.W * Kh(data.U - x, mH);
	}

	return S2 / S1;
}

void Experiment::CalculateBandwidth()
{

}

double Experiment::LOO(double h)
{
	const std::vector<DataModel::Data>& dataVector = mDataModel->GetData();
	double result = 0.0;
	 
	for (size_t i = 0; i < dataVector.size(); ++i)
	{
		double ah = 0.0;
		double S1 = 0.0;
		double S2 = 0.0;

		for (size_t j = 0; j < dataVector.size(); ++j)
		{
			if (i != j)
			{
				const DataModel::Data& data = dataVector[j];
				S1 += Kh(data.U - dataVector[i].U, mH);
				S2 += data.X * Kh(data.U - dataVector[i].U, mH);
			}
		}
		S1 /= double(dataVector.size() - 1);
		S2 /= double(dataVector.size() - 1);
		if (fabs(S1) > std::numeric_limits<double>::epsilon())
		{
			ah += S2 / S1;
		}

		double z = ah - dataVector[i].X ;
		result += z * z;
	}

	return result;
}

double Experiment::Kh(double x, double h)
{
	return 1.0 / h * mKernel(x / h);
}