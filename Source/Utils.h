#pragma once
#include <StdAfx.h>

namespace Utils
{

std::string LoadTextFromFile(const std::string& filename);
glm::vec4 RandomColor();
uint32_t ColorFromHexString(const std::string& hexString);
glm::vec4 UnormToFloatColor(uint32_t color);
GLuint CompileProgramFromFiles(const std::string& vertexShaderFilename, const std::string& pixelShaderFilename);

}