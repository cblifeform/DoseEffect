#pragma once
#include <MainWindow.h>

class Application : public wxApp
{
public:
	bool OnInit() override;

private:
	MainWindow*	mMainWindow = nullptr;
};

wxDECLARE_APP(Application);